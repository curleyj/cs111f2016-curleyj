//Jocelyn Curley
// September 14, 2016
// Class excercise
//
import java.util.Scanner; 

public class Age
{
	public static void main (String[] args)
	{
		double age; // declaration
		age = 20; // assignment

		double result = 0; // declaration and assignment
		
		// declare Scanner object
		Scanner userInput = new Scanner(System.in); 

		// prompt user for input
		System.out.println("Enter an age: ");
		age = userInput.nextDouble();

		System.out.println("You entered: "+age);

		// convert age to minutes
		result = age*365*24*60;
		System.out.println("Age in minutes: "+result);	
	}
}
