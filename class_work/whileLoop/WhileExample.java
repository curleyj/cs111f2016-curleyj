// Jocelyn Curley
// October 26, 2016

public class WhileExample
{
    // instance variable
    private int num;

    // constructor
    public WhileExample(int n)
    {
        num = n;
    }

    // set method
    public void setNum (int n)
    {
        int count = 0;
        while(count < 1000)
        {
            num += n;
            System.out.println("Iteration "+count+": new number is "+num);
            // count++
        }
    }
}
