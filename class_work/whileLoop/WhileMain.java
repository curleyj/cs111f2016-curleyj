// October 26, 2016
// Jocelyn Curley
//
import java.util.Scanner;

public class WhileMain
{
    public static void main (String args [])
    {
        Scanner scan = new Scanner (System.in);

        System.out.println("Enter a positive integer");
        int count = 0;
        int number = 0;

        number = scan.nextInt();

        // validate user input to ensure we get a positive number
        while(number < 0)
        {
            System.out.println("You need to enter a positive number");
            number = scan.nextInt();
            System.out.println("Iteration "+count+": user's input: "+number);
            count++;
        }
        // create an instance of WhileExample
        WhileExample numIterate = new WhileExample(0);
        numIterate.setNum(number);
    }
}
