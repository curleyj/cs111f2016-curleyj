// Jocelyn Curley
// November 9, 2016
//
import java.util.Scanner;
import java.text.DecimalFormat;

public class SwitchMain
{
    public static void main (String args[])
    {
        double left, right;
        char op;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter a simple expression: ");
        left = in.nextDouble();
        op = in.next().charAt(0);
        right = in.nextDouble();

        System.out.print (left+" "+op+" "+right+" = ");
    }
}
