// Jocelyn Curley
// The program takes the numbers the user gives and puts them in numerical order.
//
import java.util.Scanner;

public class Order
{
    public static void main (String args[])
    {
        Scanner input = new Scanner(System.in);
        int x, y, z;

        System.out.println("Enter three numbers: ");
        x = input.nextInt();
        y = input.nextInt();
        z = input.nextInt();
        System.out.println("You entered: "+x+" , "+y+" , "+z);

        if(x<y && x<z)
        {
           System.out.println("Inside the first if statement.");
            if(y<z)
            {
                System.out.println(x+" , "+y+" , "+z);
            }
            else{
                System.out.println(x+" , "+z+" , "+y);
            }
           System.out.println("End of first if statement.");
        }
        else if(x<y && x<z && y>z)
        {
            System.out.println(x+" , "+z+" , "+y);
        }
        else if(y<x && y<z && x<z)
        {
            System.out.println(y+" , "+x+" , "+z);
        }
        else if(y<x && y<z && x>z)
        {
            System.out.println(y+" , "+z+" , "+x);
        }
        else if(z<x && z<y && x<y)
        {
            System.out.println(z+" , "+x+" , "+y);
        }
        else if(z<x && z<y && x>y)
        {
            System.out.println(z+" , "+y+" , "+x);
        }
    }
}
