import javax.swing.*;
import java.awt.*;

public class ActionMain
{
    public static void main (String [] args)
    {
        JFrame frame = new JFrame("Janyl's Awesome Conversion");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setLayout(new FlowLayout());

        JButton button = new JButton("Convert!");

        JLabel label1 = new JLabel("Enter a temp in F: ");
        JTextField text1 = new JTextField(5);

	JLabel label2 = new JLabel("Temp in C: ");
	JLabel label3 = new JLabel("--");

        button.addActionListener(new SetTextListener(text1,label3));

	JPanel p = new JPanel();
	p.setBackground(Color.red);

	JPanel p1 = new JPanel();
	p1.setBackground(Color.green);

	
        p.add(label1);
        p.add(text1);
        p.add(button);
        p1.add(label2);
        p1.add(label3);
	frame.add(p);
	frame.add(p1);

        frame.pack();
        frame.setVisible(true);
    }
}
