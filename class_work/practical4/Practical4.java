/************************************
  Honor Code: This work is mine unless otherwise cited.
  Jocelyn Curley
  CMPSC 111
  30 September 2016
  Practical 4

  Basic Input with a dialog box
 ************************************/

import javax.swing.JOptionPane;
import java.util.Random;

public class Practical4
{
    public static void main ( String[] args )
    {
        int age = 0; // user's input - age
        int age1 = 20; // modified age

        Random random = new Random();

        // display a dialog with a message
        JOptionPane.showMessageDialog( null, "Running from the government? Need a new identity? You came to the right place!" );


        // prompt user to enter the first name
        String name = JOptionPane.showInputDialog("Please enter your first name: ");

        //create a message with the modified first name
        String newName = "Your new first name is "+ name+"lynn";

        //display the message with the modified user's name
        JOptionPane.showMessageDialog(null, newName);

        String lastName = JOptionPane.showInputDialog("Please enter your last name: ");

        String NewlastName = "Your new last name is "+ lastName+"ski";
        JOptionPane.showMessageDialog(null, NewlastName);

        // TO DO: prompt the user to get the last name, modify the last name and display the new last name

        // prompt user to enter the age
        age = Integer.parseInt(JOptionPane.showInputDialog("What is your age?"));

        // modify the age
        age1 += random.nextInt(40);
        String newAge = "Your age is: "+age1;

        // display a message with the new age
        JOptionPane.showMessageDialog(null, newAge);

        // TO DO: modify the above questions/answers to your own
        // TO DO: come up with your own (at least three) questions and answers
        //
        String occupation = JOptionPane.showInputDialog("What is your occupation?");
        String newOccupation = "Your new occupation is "+ occupation+" parent";
        JOptionPane.showMessageDialog(null, newOccupation);

        String pet = JOptionPane.showInputDialog("What is your favorite animal?");
        String newpet = "Congratulations! You now have"+pet+"as your pet!";
        JOptionPane.showMessageDialog(null, newpet);

        String place = JOptionPane.showInputDialog("What is your ideal place to live?");
        String newPlace = "Your new place to live is South" +place+"";
        JOptionPane.showMessageDialog(null, newPlace);

    } //end main
}  //end class Practical4
