// Jocelyn Curley
// November 7, 2016
//
public class Loops
{
    public static void main (String args[])
    {
        // while loop
        int count = 0;
        while(count < 100)
        {
            System.out.println(count);
            count+=10;
        }

        // do while
        count =0;
        do
        {
            System.out.println(count);
            count += 10;
        }
        while(count < 100);

        // for loop
        for (int counter = 0; counter < 100; counter+=10)
        {
            System.out.println(count);
        }

        // nested loops
        for (int i=0; i < 2; i++)
        {
            for(int j=0; j<3; j++)
            {
                for(int k=0; k < 4; k++)
                {
                    System.out.println("i = "+i+", j = "+j+", k = "+k);
                }
            }
        }
    }
}
