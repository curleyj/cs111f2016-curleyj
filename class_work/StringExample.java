//
// Jocelyn Curley
// 09 26 2016
//
import java.util.Scanner;

public class StringExample
{
    public static void main (String args[])
    {
        // declaration and assignments
        String word = ""; // create an empty String
        int length = 0;
        char character = '';
        int index = -0;
        Scanner scan = new Scanner (System.in);

        // get an input from the user
        System.out.println("Enter a String");
        word = scan.nextLine();

        System.out.println(word);

        // get the length of the string
        length = word.length();
        System.out.println("length: "+length);

        character = word.charAt(5);
        System.out.println("character at position 5: "+character);

        index = word.indexOf('a');
        System.out.println("index of character 'a' : "+index);

    }
}
