// Jocelyn Curley
// October 3, 2016
//
public class Math
{
    public static void main (String[] args)
    {
       System.out.println("max"+ Math.max(74, 928));
       System.out.println("min" + Math.min(54, 3));
       System.out.println("pow" + Math.pow(7, 8));
       System.out.println("floor" + Math.floor(3.780));
       System.out.println("ceil" + Math.ceil(35.321));

    }
}
