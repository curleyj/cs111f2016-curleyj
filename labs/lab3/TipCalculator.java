//
// Honor Code: This work is mine unless otherwise cited.
// Jocelyn Curley
// CMPSC 111 Fall 2016
// Lab #3
// Date: 09 15 2016
//
// Purpose: To calculate the tip, the total bill for the user, and each person's share of the bill.
//
import java.util.Scanner;
import java.util.Date;

public class TipCalculator
{
	public static void main (String[] args)
	{
	
	System.out.println("Jocelyn Curley\n Lab #3\n" + new Date() + "\n");
	
	// declare Scanner object
	Scanner userInput = new Scanner(System.in);
	double amount = 0;
	double tip;
	double bill;
	double totalbill;
	double people;
	double splitbill;
	String name;
	
	// prompt user for input
	System.out.println("Please enter your name: ");
	name = userInput.next();
	
	System.out.println("Welcome to the Tip Calculator!");
	
	System.out.println("Please enter the amount of your bill: ");
	bill = userInput.nextDouble();
	
	System.out.println("Please enter the percentage that you want to tip: ");
	tip = userInput.nextDouble();
	
	System.out.println("Your original bill was"+bill);
	
	tip = tip/100*bill;
	
	System.out.println("Your tip amount is: "+tip);
	
	totalbill = tip+bill;
	
	System.out.println("Your total bill is: "+totalbill);
	
	System.out.println("How many people will be splitting the bill?: ");
	people = userInput.nextDouble();
	
	splitbill = totalbill/people;

	System.out.println("Each person should pay: "+splitbill);

	System.out.println("Have a nice day! Thank you for using our service.");
	}
}
	
	
	
	
	
