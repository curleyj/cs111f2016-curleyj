// Honor Code: This work is ours unless otherwise cited.
// Jocelyn Curley & Laura McClain
// Date: October 27, 2016
// CMPSC 111 Fall 2016
// Lab # 7
//

public class TodoItem {

    private int id;
    private static int nextId = 0;
    private String priority;
    private String category;
    private String task;
    private boolean done;

    public TodoItem(String p, String c, String t) {
        id = nextId;
        nextId++;
        priority = p;
        category = c;
        task = t;
        done = false;
    }

    public int getId() {
        return id;
    }

    public String getPriority() {
        return priority;
    }

    public String getCategory() {
        return category;
    }

    public String getTask() {
        return task;
    }

    public void markCompleted() {
        done = true;
    }

    public boolean isCompleted() {
        return done;
    }

    public String toString() {
        return new String(id + ", " + priority + ", " + category + ", " + task + ", completed? " + done);
    }

}
