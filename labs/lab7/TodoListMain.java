// Honor Code: This work is ours unless otherwise cited.
// Jocelyn Curley & Laura McClain
// Date: October 27, 2016
// CMPSC 111 Fall 2016
// Lab # 7
//

import java.io.IOException;
import java.util.Scanner;

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, completed, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();


        while(scanner.hasNext()) {
            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            else if(command.equals("completed")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsCompleted(chosenId);
            }
            else if(command.equals("quit")) {
                break;
            }
            else if(command.equals("priority-search")) {
                System.out.println(todoList.toString());
                System.out.println("What priority are you searching for?");
                String priority = scanner.nextLine();
                todoList.findTasksOfPriority(priority);
            }
            else if(command.equals("category-search")) {
                System.out.println(todoList.toString());
                System.out.println("What category are you searching for?");
                String category = scanner.nextLine();
                todoList.findTasksOfCategory(category);

            }
        }

    }

}
