I worked with Laura McClain on this lab.
The challenges we faced were when we were confused on how to fix a problem,
we weren't always sure where the problem was. Also sometimes we got confused
about how each method worked with each other. We had a lot of problems
getting the priority search to work, but eventually we fixed it.
I wrote all of the code while Laura dictated to me what to write. And we 
bounced ideas off of each other in order to fix the problems we encountered.

