
// Honor Code: This work is mine unless otherwise cited.
// Jocelyn Curley
// CMPSC 111 Fall 2016
// Lab # 2
// Date: 09 08 2016
//
// Purpose: Creating a template to use for all other labs, and to practice using java
//
import java.util.Date; //

public class WordsofWisdom2
{
	//
	// main method: program execution begins here
	//
	public static void main (String[] args)
	{
	// Label output with name and date:
	System.out.print("Jocelyn Curley\n Lab # 2\n" + new Date() + "\n");
	System.out.print(("The marks humans leave are too often scars") + "\n");
	System.out.print(("Like most misery, it started with apparent happiness") + "\n");
	}
}

