// Jocelyn Curley
// Melissa McCann
// Honor Code: This work is ours unless otherwise cited.
// CMPSC 111 Fall 2016
// Lab 6
// October 6, 2016
//
// Purpose: Creating music
//
package lab6;
import java.util.Date;
import org.jfugue.player.Player;
import org.jfugue.pattern.Pattern;
import java.util.Scanner;


public class Music_Lab {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		System.out.println("Jocelyn Curley and Melissa McCann\nLab 6\n" + new Date() + "\n");
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Welcome to Music Magic!");
		
		System.out.println("Choose an instrument. Your choices are Flute, Violin, Xylophone, Electric Grand, or Synth Drums. ");
		
		System.out.println("Choose a tempo. Your choices are Allegro, Moderato, Largo, Presto, and Andante.");
		
		String instrument = scan.nextLine();
		instrument = instrument.toLowerCase();
		
		String tempo = scan.nextLine();
		
		if (instrument.equals("flute") || instrument.equals("violin") || instrument.equals("piano") || instrument.equals("xylophone") || instrument.equals("electric piano") || instrument.equals("synth drums"));
			System.out.println("You chose: " +instrument);
		
		if (tempo.equals("Allegro") || tempo.equals("Moderato") || tempo.equals("Largo") || tempo.equals("Presto") || tempo.equals("Andante"));
			System.out.println("You chose: " +tempo);
			
		Player player = new Player();
	
		Pattern pattern = new Pattern("I[" +instrument+ "] " +tempo+ "C60q C60q G67q G67q A69q A69q G67h F65q F65q E64q E64q D62q D62q C60h G67q G67q F65q F65q E64q E64q D62h G67q G67q F65q F65q E64q E64q D62h C60q C60q G67q G67q A69q A69q G67h F65q F65q E64q E64q D62q D62q C60h");
		Pattern pattern2 = new Pattern("I[" +instrument+ "] " +tempo+ "E64q E64q E64h E64q E64q E64h E64q G67q C60q D62q E64w F65q F65q F65q F65q F65q E64q E64q E64q E64q D62q D62q E64q D62h-G67h E64q E64q E64h E64q E64q E64h E64q G67q C60q D62q E64w F65q F65q F65q F65q F65q E64q E64q E64q G67q G67q F65q D62q C60w");
		Pattern pattern3 = new Pattern("I[" +instrument+ "]" +tempo+ "D62q E64q D62q C60q B59q C60q D62h A57q B59q C60h B59q C60q D62h D62q E64q D62q C60q B59q C60q D62h A57h D62h B59q G55w");
		
		player.play(pattern);
		player.play(pattern2);
		player.play(pattern3);
		
	}

}
