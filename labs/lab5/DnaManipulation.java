
// Honor Code: This work is mine unless otherwise cited.
// Jocelyn Curley and Laura McClain
// CMPSC 111 Fall 2016
// Lab # 5
// Date: 09 29 2016
//
// Purpose: Creating a program to run DNA Manipulation
//
import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class DnaManipulation
{
	//
	// main method: program execution begins here
	//
	public static void main(String[] args)
	{
        // variables
        Scanner input = new Scanner(System.in);
        Random rand = new Random();
        String s1, dnaString;
        char c;
        int position = 0;
        int length = 0;

		// Label output with name and date:
		System.out.println("Jocelyn Curley&Laura McClain\n Lab # 5\n" + new Date() + "\n");

        System.out.println("Enter a string of DNA only using the letters A, C, G, and T (no spaces): ");
        s1 = input.next();

        s1 = s1.toLowerCase();

        dnaString = s1.replace('a', 'T');
        dnaString = s1.replace('t', 'A');
        dnaString = s1.replace('c', 'G');
        dnaString = s1.replace('g', 'C');

        c = s1.charAt(rand.nextInt(4));
        position = rand.nextInt(length+1);

        System.out.println("Inserting "+c+" at position "+position+ " gives: " +dnaString.substring(0, position)+c+dnaString.substring(position));

        System.out.println("Deleting from position"+position+" gives: "+dnaString.substring(position));

        System.out.println("Changing position "+position+" gives: "+dnaString.substring(0, position)+c+dnaString.substring(position+1));

	}
}
