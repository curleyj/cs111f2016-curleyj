
// Honor Code: This work is mine unless otherwise cited.
// Jocelyn Curley
// CMPSC 111 Fall 2016
// Lab # 8
// Date: November 10, 2016
//
// Purpose: Creating a carpet design
//
import java.util.Date;
import java.util.Scanner;

public class CarpetLab
{
	//
	// main method: program execution begins here
	//
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Jocelyn Curley\n Lab # 8\n" + new Date() + "\n");

       System.out.println("Enter a character for design");

        Scanner in = new Scanner(System.in);

        char design = in.next().charAt(0);

        for(int i=0; i<3; i++)
        {
            for(int j=0; j<32; j++)
            {
                System.out.print(design);
            }
            System.out.println();
        }

        System.out.println();

        for(int i=0; i<32; i++)
        {
            System.out.print(design);
        }
        System.out.println();

        for(int i=0; i<3; i++)
        {
            for(int j=0; j<8; j++)
            {
                System.out.print(""+design+design+"  ");
            }

            System.out.println();

            for(int k=0; k<8; k++)
            {
                System.out.print(""+"  "+design+design);
            }

            System.out.println();
        }

        for(int i=0; i<1; i++)
        {
            for(int j=0; j<1; j++)
            {
                System.out.print("               "+design+"               ");
            }
        }

        System.out.println();

        for(int i=0; i<3; i++)
        {
            for(int j=0; j<8; j++)
            {
                System.out.print(""+"  "+design+design);
            }

            System.out.println();

            for(int k=0; k<8; k++)
            {
                System.out.print(""+design+design+"  ");
            }

            System.out.println();
        }

        for(int i=0; i<32; i++)
        {
            System.out.print(design);
        }
        System.out.println();

        for(int i=0; i<3; i++)
        {
            for(int j=0; j<32; j++)
            {
                System.out.print(design);
            }
            System.out.println();
        }

    }
}


