// Jocelyn Curley
// Melissa McCann
// Honor Code: This work is ours unless otherwise cited.
// CMPSC 111 Fall 2016
// Lab 6
// October 6, 2016
//
// Purpose: Creating music
//
import java.util.Date;
import org.jfugue.player.Player;
import org.jfugue.pattern.Pattern;
import java.util.Scanner;
import java.util.Random;

public class Music_Lab {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		System.out.println("Jocelyn Curley and Melissa McCann\nLab 6\n" + new Date() + "\n");
		
		Scanner scan = new Scanner(System.in);
		Random song = new Random();
		int patternChoice = 0;
		
		System.out.println("Welcome to Music Magic!");
		
		System.out.println("Choose an instrument. Your choices are Flute, Violin, Xylophone, Timpani, or Guitar. ");
		
		String instrument = scan.nextLine();
		instrument = instrument.toLowerCase();
		
		if (instrument.equals("flute") || instrument.equals("violin") || instrument.equals("piano") || instrument.equals("xylophone") || instrument.equals("timpani") || instrument.equals("guitar"));
			System.out.println("You chose: " +instrument);
		
			System.out.println("Choose a tempo. Your choices are Allegro, Moderato, Largo, Presto, and Andante.");
			
		String tempo = scan.nextLine();
			
		if (tempo.equals("Allegro") || tempo.equals("Moderato") || tempo.equals("Largo") || tempo.equals("Presto") || tempo.equals("Andante"));
			System.out.println("You chose: " +tempo);
			
		Player player = new Player();
		patternChoice = song.nextInt(3);
		
		if (patternChoice == 0) {
			Pattern pattern = new Pattern("I[" +instrument+ "] T["+tempo+ "] 60q 60q 67q 67q 69q 69q 67h 65q 65q 64q 64q 62q 62q 60h 67q 67q 65q 65q 64q 64q 62h 67q 67q 65q 65q 64q 64q 62h 60q 60q 67q 67q 69q 69q 67h 65q 65q 64q 64q 62q 62q 60h");
			player.play(pattern);
		} else if (patternChoice == 1) {
			Pattern pattern2 = new Pattern("I[" +instrument+ "] T["+tempo+ "] 64q 64q 64h 64q 64q 64h 64q 67q 60q 62q 64w 65q 65q 65q 65q 65q 64q 64q 64q 64q 62q 62q 64q 62h-67h 64q 64q 64h 64q 64q 64h 64q 67q 60q 62q 64w 65q 65q 65q 65q 65q 64q 64q 64q 67q 67q 65q 62q 60w");
			player.play(pattern2);
		} else if (patternChoice == 2) {
			Pattern pattern3 = new Pattern("I[" +instrument+ "] T["+tempo+ "] 62q 64q 62q 60q 59q 60q 62h 57q 59q 60h 59q 60q 62h 62q 64q 62q 60q 59q 60q 62h 57h 62h 59q 55w");
			player.play(pattern3);
		}
		
		System.out.println("Thanks for making music with us!");
		scan.close();
		
	}
}