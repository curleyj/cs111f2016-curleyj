//
//Honor code: This work is mine unless otherwise cited.
//Jocelyn Curley
//09 22 2016
//
//Purpose: calculate a square root
//
import java.util.Date;
import java.util.Scanner;

public class SquareRoot
{
    //
    // main method: program execution begins here
    //
    public static void main(String[] args)
    {
        System.out.println("Jocelyn Curley\n Lab4\n" + new Date() + "\n");
        System.out.println("What number are you trying to calculate the square root of?");

        Scanner userInput = new Scanner(System.in);
        int number = userInput.nextInt();

        System.out.println("Make a guess of what you think the square root is");
        double guess = userInput.nextInt();

        System.out.println("Replace guess " +guess);
        guess = .5*(guess+number/guess);

        System.out.println("Replace guess " +guess);
        guess = .5*(guess+number/guess);

        System.out.println("Replace guess " +guess);
        guess = .5*(guess+number/guess);
    }
}
