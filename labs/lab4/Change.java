//
// Honor Code: This work is mine unless otherwise cited.
// Jocelyn Curley
// Lab 4
// Date: 09 22 2016
// Purpose: Making a program to make change.
//
import java.util.Date;

public class Change
{
    //
    // main method: program execution starts here
    //
    public static void main(String[] args)
    {
        System.out.println("Jocelyn Curley\n Lab #4\n" + new Date() + "\n" );

        int cents = 7896;

        int tens; // number of ten-dollar bills needed
        int fives; // number of five-dollar bills needed
        int ones; // number of one-dollar bills needed
        int quarters; // number of quarters needed
        int dimes; // number of dimes needed
        int nickles; // number of nickles needed
        int pennies; // number of pennies needed

        System.out.println("To make change for " + cents + " cents, you need:");

        tens = cents / 1000;
        cents = cents % 1000;

        System.out.print(tens +" tens, ");

        fives = cents / 500;
        cents = cents % 500;

        System.out.print(fives +" fives, ");

        ones = cents / 100;
        cents = cents % 100;

        System.out.print(ones +" ones, ");

        quarters = cents / 25;
        cents = cents % 25;

        System.out.print(quarters +" quarters, ");

        dimes = cents / 10;
        cents = cents % 10;

        System.out.print(dimes +" dimes, ");

        nickles = cents / 5;
        cents = cents % 5;

        System.out.print(nickles +" nickles, ");

        pennies = cents / 1;
        cents = cents % 1;

        System.out.print(pennies +" pennies, ");


    }
}
