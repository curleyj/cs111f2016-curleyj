// Jocelyn Curley
What are five ways in which it is now different from the "default" configuration that you were using previously? It is different in that it has different color schemes. Personally I like the evening one. Also, when you want to do a declaration line after you hit enter it continues so you don't have to type in "//" everytime. Also when you hit space it has a little dot next to the word you just typed until you start typing again. Another thing is that it autofills after you start typing something. Lastly if you use quotes or brackets it will autofill the end quote or bracket so you don't have to type it.
Knowing these facts, what do you think the "gg=G" command does? I think it would indent the line.
How do these commands allow you to manipulate your Java program? It allows you to waste less time typing everything out individually.

