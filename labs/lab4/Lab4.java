//
// Honor Code: This work is mine unless otherwise cited.
// Jocelyn Curley
// CMPSC 111 Fall 2016
// Lab #4
// Date: 09 22 2016
//
// Purpose:
//
import java.util.Date;

public class Lab4
{
    //
    // main method:
    //
    public static void main(String[] args)
    {
        System.out.println("Jocelyn Curley\n Lab #4\n" + new Date() + "\n");
        System.out.println("This is the coolest. I love all the different color schemes.");

    }

}

