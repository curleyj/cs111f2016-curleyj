//
// Honor Code: This work is mine unless otherwise cited.
// Jocelyn Curley
// Practical 3
// CMPSC 111 Fall 2016
// September 16, 2016
//
import java.util.Date;
import java.util.Scanner;

public class MadLibs
{
	public static void main(String[] args)
	{
		System.out.println("Jocelyn Curley\n Practical 3\n" + new Date() + "\n");
		// declare Scanner object
		Scanner userInput = new Scanner(System.in);
		String singularnoun;
		String adjective;
		String anotheradjective;
		double notzerowholenumber;
		double anothernotzerowholenumber;
		double anynumber;
		String singularverb;
		double answer;

		System.out.println("Enter a singular noun: ");
		singularnoun = userInput.next();
		
		System.out.println("Enter an adjective: ");
		adjective = userInput.next();

		System.out.println("Enter another adjective: ");
		anotheradjective = userInput.next();

		System.out.println("Enter a non-zero whole number: ");
		notzerowholenumber = userInput.nextDouble();

		System.out.println("Enter another non-zero whole number: ");
		anothernotzerowholenumber = userInput.nextDouble();

		System.out.println("Enter any number: ");
		anynumber = userInput.nextDouble();

		System.out.println("Enter a singular verb ");
		singularverb = userInput.next();

		System.out.println("Third-Grade Word Problem");

		System.out.println("If you own" +notzerowholenumber+" "+adjective+" "+singularnoun+"s, and you purchase" +anothernotzerowholenumber+anotheradjective+singularnoun+"s, how many more" +singularnoun+"s do you need to" +singularverb+" "+anynumber+"?" );

		answer = notzerowholenumber+anothernotzerowholenumber-anynumber;

		System.out.println("Answer: You need" +answer+ "more" +singularnoun+"s.");
	}
}


