// Jocelyn Curley
// 11/4/2016
// Practical 9
// Honor Code: This work is mine unless otherwise cited.
//
import java.util.*;

public class BookReader
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int numWords = 0;
        while(scan.hasNext())
        {
            String x = scan.next();
            numWords++;
        }
        System.out.println("Number of words: " + numWords);

        ArrayList<String> words = new ArrayList<String>();
        while(scan.hasNext())
        {
            String y = scan.next().toLowerCase();
            if(!words.contains(y))
            {
                words.add(y);
            }
        }
        System.out.println("Number of distinct words: " + words.size());
    }
}
