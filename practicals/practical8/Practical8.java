//====================================
// Jocelyn Curley
// Honor Code: This work is mine unless otherwise cited.
// CMPSC 111
// Practical 8
// 28 October 2016
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical8
{
    public static void main(String[] args)
    {
        System.out.println("Janyl Jumadinova\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // an octopus
        Utensil spat;           // a kitchen utensil

        Octopus michael;
        Utensil fork;

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        ocky = new Octopus("Ocky", 50);    // create and name the octopus
       // ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        fork = new Utensil("fork");
        fork.setColor("purple");
        fork.setCost(3.99);

        michael = new Octopus("Michael", 20);
        michael.setUtensil(fork);
        michael.setWeight(130);

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        System.out.println("Testing 'get' methods;");
        System.out.println(michael.getName() + "weighs " +michael.getWeight()
            + " pounds\n" + "and is " + michael.getAge()
            + " years old. His favorite utensil is a "
            + michael.getUtensil());

        System.out.println(michael.getName() + "'s " + michael.getUtensil() + "costs $"
            + michael.getUtensil().getCost());
        System.out.println("Utensil's color: " + fork.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        michael.setAge(21);
        michael.setWeight(107);
        fork.setCost(13.95);
        fork.setColor("black");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());

        System.out.println("\nTesting 'set' methods:");
        System.out.println(michael.getName() + "'s new age: " + michael.getAge());
        System.out.println(michael.getName() + "'s new weight: " + michael.getWeight());
        System.out.println("Utensil's new cost: $" + fork.getCost());
        System.out.println("Utensil's new color: " + fork.getColor());
    }
}
