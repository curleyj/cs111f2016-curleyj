
// Honor Code: This work is mine unless otherwise cited.
// Jocelyn Curley
// CMPSC 111 Fall 2016
// Practical 5
// Date: 10 07 2016
//
// Purpose: Creating a program to determine what events happened in a year given by the user.
//
import java.util.Date; //
import java.util.Scanner;

public class Years
{
	//
	// main method: program execution begins here
	//
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Jocelyn Curley\n Practical 5\n" + new Date() + "\n");

        Scanner userInput = new Scanner(System.in);
        int year = 0;

        System.out.println("Welcome to the one and only Year Checker!");
        System.out.println("Enter a year between 1500 and 3000: ");
        year = userInput.nextInt();

        if (year%4 == 0)
            System.out.println(year+ " is a leap year.");
        else
            System.out.println(year+ " is not a leap year.");
        if (year%17 == 0)
            System.out.println(year+ " is an emergence year for cicadas.");
        else
            System.out.println(year+ " is not an emergence year for cicadas.");
        if (year%11 == 0)
            System.out.println(year+ " is a peak sunspot year.");
        else
            System.out.println(year+ " is not a peak sunspot year.");

        System.out.println("Thank you for using our Year Checker! Have a wonderful Day!");

	}
}
