//
// Practical 2
// 9 September 2016
// Prints Bunny
// Honor Code: This work is mine unless otherwise cited.
//
import java.util.Date;
public class Interesting
{

	public static void main(String[] args)
	{
		System.out.println("Jocelyn Curley, CMPSC 111\n" + new Date() + "\n");
		System.out.println("  ()  ()");
		System.out.println("  (o . o)");
		System.out.println(" /(     )\\");
		System.out.println("  (     )o");
	}
}
