//*********************************************************************************
// CMPSC 111 Fall 2016
// Practical 10
//
// Purpose: Program demonstrating reading input from the terminal
//*********************************************************************************
import java.util.Scanner;

public class SwitchDay
{
    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.print( "Enter a day of the week: "  );
        String day = s.nextString();

        String character = s.nextString();

        switch ( character )
        {
            case "friday": case "Friday":
            case "monday": case "Monday":
            case "tuesday": case "Tuesday":
            case "Wednesday": case "Wednesday":
            case "thursday": case "Thursday":
                System.out.print(character+" is a weekday\n");
                break;
            default:
                System.out.print(character+" is not a weekday\n");

        }

    // TO DO: Using a switch statement, given the day of the week, print whether it is a weekday or a weekend day.
    }
}
