// Define class GradeBook with a member method displayMessage

public class GradeBook {

    // variable declarations - instance variables
    String courseName = "CMPSC 111";

    //System.out.println("inside GradeBook");
    // method to display a welcome message
    // parameter - courseName
    public void displayMessage(String courseName) {
        System.out.println("Welcome to the Grade Book for "+courseName);
    }

    public String getCourseName() {
        return courseName;
    }
    //System.out.println("end of GradeBook");
}
