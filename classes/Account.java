/*
Account class that creates an Account
It has a constructor and two methods - credit and getBalance
*/

public class Account
{  
    // instance variable
    private double balance;

    // constructor
    public Account ( double initialBalance )
    {
        // initialize
        balance = initialBalance;
        
    }

    // set method, changes the value of the variable 'balance'
    public void credit ( double amount )
    {
        balance = balance + amount;
    }
    
    //get method, returns the value of the variable 'balance'
    public double getBalance ()
    {
        return balance;
    }
}
